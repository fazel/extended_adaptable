<?php
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2017072506;
$plugin->requires  = 2016120500;
$plugin->component = 'theme_extended_adaptable';
$plugin->release = '0.1';
$plugin->maturity = MATURITY_STABLE;
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2014111000,
    'theme_adaptable' => 2017072500,
);
