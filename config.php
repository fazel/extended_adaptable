<?php

defined('MOODLE_INTERNAL') || die();

global $PAGE;

$THEME->name = 'extended_adaptable';
$THEME->parents = array('bootstrapbase', 'adaptable');

$THEME->rendererfactory = 'theme_overridden_renderer_factory';

$THEME->sheets = Array();
$THEME->yuicssmodules = array();
$THEME->enable_dock = true;
$THEME->editor_sheets = array();